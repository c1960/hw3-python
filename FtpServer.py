#!/usr/bin/python3

# CS 472 - HW3
# Christopher S. Good, Jr. 
# FtpServer.py

from socket import create_server, timeout
from threading import Event, Thread, get_ident
from FtpService import FtpService
from MessageEncoder import MessageEncoder
from Logger import Logger


class FtpServer(Thread):
    '''
    FTP Server Wrapper

    Wrapper for an FTP Server that implements the basic commands with error handling.
    This server is designed to run as a Thread so that the Server is non-blocking. This
    means that the calling program has control of starting and stopping the Server.

    Additionally, this Server inherits from the Thread class, which allows it to behave
    like a `Thread` with `start()` and `join()` methods acting as the main entry and exit
    points. There is also a flag (`server_running`) that controls the main Server while loop. 
    This flag can be used to forcefully halt the main Server process.
    '''

    def __init__(self, address: str = '192.168.1.169', port: int = 2121, logger: Logger = Logger(), encoder: MessageEncoder = MessageEncoder()):
        '''
        Initializes the FTP Server

        Parameters
        ----------
        `logger`: Logger => custom logger for the FTP Server
        `encoder`: MessageEncoder => custom message encoder/decoder
        '''
        Thread.__init__(self)
        self.machineAddress: str = address
        self.machinePort: int = port
        self.state = None
        self.logger: Logger = logger
        self.encoder: MessageEncoder = encoder
        self.server_running = Event()
        self.connections: list[FtpService] = []

    def __del__(self):
        '''
        Destruct FTP Server

        Called when the FTP Server loses scope or is Garbage Collected.
        '''
        print('Exiting FTP Server.')

    def run(self):
        '''
        Main Server Process

        Using the Machine's Address and Port, the server is started using the `socket`
        `create_server` function. Using the connection from the server, and while the 
        `server_running` flag is not set from the main process, this method will run 
        continuously and accept new connections as they provided to the server. Each 
        new connection is used to spawn a new FTP Service, which runs on another Thread
        so that it is non-blocking. 
        '''
        self.sendStart()  # Run Starting Sequence
        with create_server((self.machineAddress, self.machinePort)) as server:
            # Set Timeout to 1 so Process does not hang on Kill Command
            server.settimeout(1)
            a, p = server.getsockname()
            self.logger.log(
                'INFO', get_ident(), f'Server is hosted at Address: {a} on Port: {p}')
            while not self.server_running.is_set():
                conn = addr = None

                try:
                    # Accept new connections from Clients
                    conn, addr = server.accept()
                except timeout:
                    # If connection times out, continue with the while loop
                    continue

                if conn and addr:
                    self.logger.log(
                        'INFO', get_ident(), f'Socket connection accepted from {addr}!')
                    service = FtpService(conn, addr[0], self.logger)
                    self.connections.append(service)
                    service.start()  # Start Service Thread
                else:
                    self.logger.log(
                        'WARNING', get_ident(), 'Connection or Address are not valid!')
            self.logger.log(
                'INFO', get_ident(), 'Reached the end of the main function!')

    def sendStart(self):
        '''
        Server Starting Sequence

        Performs various logging tasks to show that the server is alive.
        '''
        self.logger.log('INFO', get_ident(), 'Starting server...')

    def sendStop(self):
        '''
        Server Stopping Sequence

        Performs various logging tasks to show that the server is stopping, as well as
        handling the cleanup of the loose connections that may be left on the Server.
        '''
        self.server_running.set()
        self.stop()

    def stop(self):
        '''
        Server Connection Cleanup

        Iterates through the list of connections (FTP Services) that are still being
        managed by the Server. If a connection is alive, it is stopped and joined to 
        the main process.
        '''
        self.logger.log('INFO', get_ident(), 'Stopping Server...')
        if len(self.connections):
            self.logger.log(
                'INFO', get_ident(), 'Stopping Connection Threads...')
            for connection in self.connections:
                self.logger.log(
                    'INFO', get_ident(), f'Stopping Thread: {connection.ident}')
                if connection.is_alive():
                    connection.sendClose()
                    connection.join()
                    self.logger.log('INFO', get_ident(), 'Thread joined.')
                else:
                    self.logger.log(
                        'INFO', get_ident(), 'Thread is already joined')
            hanging_threads = [1 if connection.is_alive() else 0 for connection in self.connections]
            self.logger.log('INFO', get_ident(), f'Hanging Threads: {sum(hanging_threads)}')
        self.logger.log('INFO', get_ident(),
                                'All Threads have been properly terminated')
