#!/usr/bin/python3

# CS 472 - HW3
# Christopher S. Good, Jr. 
# FtpService.py

from os import name, remove, stat
from platform import system, release
from socket import AF_INET, AF_INET6, SOCK_STREAM, create_server, error, gaierror, has_dualstack_ipv6, socket
from json import load
from threading import Event, Thread, get_ident
from FileSystemHandler import FileSystemHandler
from Logger import Logger
from MessageEncoder import MessageEncoder
from FileSystemHandler import FileSystemHandler
from FtpErrorCodeHandler import FtpErrorCodeHandler

class FtpService(Thread):
    '''
    FTP Service Handler

    Wrapper for an FTP Service Handler that implements the basic commands of an FTP 
    Service. This Service is designed to run as a Thread so that the Service is
    non-blocking. This means that when the Server accepts multiple clients, it will be
    able to handle them simultaneously without having to worry about crossing data
    between Clients. 

    The service keeps track of a few different states:
    `AUTH`: Conjoined state of the login process
    `IDLE`: Server is spinning without a data connection
    `PASV`: Server is connected through a passive/extended passive connection 
    `PORT`: Server is connected through a port/extened port connection
    `CLOSED`: Not really tracked, but the thought is expressed by the `close` method
    '''

    def __init__(self, connection: socket, address: str, logger: Logger=Logger()):
        '''
        Initialized the FTP Service

        Parameters
        ----------
        `connection`: socket => client connection passed from the Server
        `address`: str => address of the server
        '''
        Thread.__init__(self, daemon=True)
        self.userName = None
        self.userPass = None
        self.state = "AUTH"
        self.userLoggedIn = False
        self.filesystem: FileSystemHandler = FileSystemHandler(logger=logger)
        self.addr: str = address
        self.connection: socket = connection
        self.dataConnection: socket = None
        self.logger: Logger = logger
        self.encoder: MessageEncoder = MessageEncoder()
        self.running = Event()
        self.error = FtpErrorCodeHandler()

    def __del__(self):
        '''
        Destruct FTP Service

        Called when the FTP Service loses scope or is Garbage Collected.
        '''
        if self.dataConnection:
            self.dataConnection.close()
        if self.connection:
            self.connection.close()

    def sendClose(self):
        '''
        Send the Close Signal 

        Starts the closing sequence of the FTP Service. Sets the running
        Thread Event Signal to True, which terminates the main process of 
        the Thread. Then, this method triggers the closing sequence.
        '''
        self.running.set()
        self.close()

    def sendStart(self):
        '''
        Service Starting Sequence

        Performs various logging and response tasks to show that the Service is up and running.
        '''
        self.logger.log('INFO', get_ident(), f'Service running Thread ID: {get_ident()}')
        self.logger.log('INFO', get_ident(), f'Running Connection Thread connected to {self.addr}')
        self.sendResponse(code=220, msg='Service ready new user')

    def handleClientCommand(self, clientCmd):
        '''
        Handle Client Commands

        Method that handles the incoming Client commands and makes sure
        that the Commands and Arguments are parsed and handled properly.
        '''
        clientCmd = clientCmd.strip()
        clientCmd = clientCmd.split(' ', 1)
        if len(clientCmd) == 2:
            return clientCmd[0], clientCmd[1]
        return clientCmd[0], None

    def run(self):
        '''
        Main Service Process

        Runs the main process for the FTP Service. While the Service is running with the `running` flag, 
        it will perpetually run through and read commands from the client. Each command will be parsed
        and then a decision will be made from the simple decision tree of commands which call the various 
        methods of the FTP Service.

        Codes
        -----
        `501`: There was an error with the command or argument
        `502`: The command is not implemented on the server
        '''
        self.sendStart() # Run Starting Sequence
        cmd = arg = None
        while not self.running.is_set():
            self.logger.log('INFO', get_ident(), f'Service State: {self.state}')
            try:
                cmd, arg = self.handleClientCommand(self.readCommand())
                self.logger.log('INFO', get_ident(), f'Received Command: {cmd} with Argument {arg} from Client')
                # USER: Capture username
                if cmd.lower() == 'user' and arg:
                    self.user(arg)
                # PASS: Capture password
                elif cmd.lower() == 'pass' and arg: 
                    self.password(arg)
                # PORT: Enter port connection
                elif cmd.lower() == 'port' and arg:
                    arg = arg.split(',')
                    self.port('.'.join(arg[0:4]), arg[4], arg[5])
                # EPRT: Enter e-port connection
                elif cmd.lower() == 'eprt' and arg:
                    _, etype, addr, port, _ = str(arg).split('|')
                    self.eprt(etype, addr, port)
                # CWD: Change directories
                elif cmd.lower() == 'cwd' and arg:
                    self.cd(arg)
                # STOR: Store document in File System
                elif cmd.lower() == 'stor' and arg:
                    self.stor(arg)
                # RETR: Send document from File System
                elif cmd.lower() == 'retr' and arg:
                    self.retr(arg)
                # PASV: Enter passive connection
                elif cmd.lower() == 'pasv':
                    self.pasv()
                # EPSV: Enter e-passive connection 
                elif cmd.lower() == 'epsv':
                    self.epsv()
                # LIST: Return list of current directory in File System
                elif cmd.lower() == 'list':
                    self.list()
                # PWD: Return the present location of user in File System
                elif cmd.lower() == 'pwd':
                    self.pwd()
                # SYST: Return system information
                elif cmd.lower() == 'syst':
                    self.syst()
                # CDUP: Change directory to parent directory in File System
                elif cmd.lower() == 'cdup':
                    self.cdup()
                # QUIT: Exit the connection with Client
                elif cmd.lower() == 'quit':
                    self.sendResponse(code=221)
                    self.close()
                    break
                else:
                    self.sendResponse(code=502)
                    self.logger.log('WARNING', get_ident(), 'Command not implemented on the server')
                    continue
            except:
                # self.sendResponse(code=501)
                # self.logger.log('ERROR', get_ident(), 'Something went wrong with the incoming command!')
                continue

    def close(self):
        '''
        Closing Sequence 

        Method that handles closing the FTP Service and asserting that all connections have been 
        properly closed and tied up. 
        '''
        self.logger.log('INFO', get_ident(), 'Closing FTP Service on Thread')
        if self.dataConnection:
            self.dataConnection.close()
            self.logger.log('INFO', get_ident(), 'Data Connection with Client Closed!')
        if self.connection:
            self.connection.close()
            self.logger.log('INFO', get_ident(), 'Control Connection with Client Closed!')

    def resetDataConnection(self) -> None:
        '''
        Reset Data Connection

        Method that closes the current Data Connection and sets the instance to 
        None (makes sure that connection socket has been removed). Also resets the 
        state to IDLE from PASV or PORT
        '''
        self.dataConnection.close()
        self.dataConnection = None 
        self.state = 'IDLE'
        self.logger.log('INFO', get_ident(), 'Data connection reset.')
            
    def readCommand(self) -> str:
        '''
        Read Client Command

        Receives a command from a Client byte by byte until a newline character is found.
        '''
        msg = []
        while True:
            try:
                bytechar = self.connection.recv(1)
            except gaierror:
                self.logger.log('WARNING', get_ident(), 'CMD: Socket Address Error')
                break
            except error:
                self.logger.log('WARNING', get_ident(), 'CMD: Data Reading Error.')
                break
            except:
                self.logger.log('ERROR', get_ident(), 'CMD: General Socket Error.')
                break
            msg.append(bytechar)
            if (bytechar == b'\n'):
                break
        return self.encoder.decode(b''.join(msg)).strip()

    def readData(self, pathname: str) -> int:
        '''
        Read Data

        Receives data from the Client in the form of a file and handles both PORT and 
        PASV connections. The data connection is opened as a read-only binary file and
        is written into the file at the designated pathname

        Parameters
        ----------
        `pathname`: str => path to read data into
        '''
        try:
            with self.dataConnection.makefile('rb') as data:
                self.sendResponse(code=125) # Transfer starting 
                try:
                    with open(pathname, 'wb') as file:
                        for line in data:
                            file.write(line)
                except:
                    return 451 # Local file error
                return 226 # Successful, closing data connection
        except:
            return 425 # Cannot open data connection

    def sendResponse(self, code: int, msg: str = None) -> None:
        '''
        Send Response

        Sends a response code and message to the Client

        Parameters
        ----------
        `code`: int => status code 
        `msg`: str => message to accompany status code
        '''
        try:
            if not msg:
                # If the message is empty then retrieve code from Code Handler 
                msg = FtpErrorCodeHandler().getByCode(code)
            self.connection.sendall(self.encoder.encode(f'{code} {msg}'))
            self.logger.log('Sent', get_ident(), f'{code}: {msg}')
        except:
            self.logger.log(code, get_ident(), 'There was an issue sending the response')

    def sendData(self, pathname: str) -> int:
        '''
        Send Data

        Send data as file to the Client

        Parameters
        ----------
        `pathname`: str => path to send data from 

        Codes
        -----
        `250`: File action completed
        `425`: Cannot open connection
        `451`: Action aborted
        '''
        try:
            with open(pathname, 'rb') as data:
                self.logger.log('INFO', get_ident(), 'File is open...')
                try:
                    self.sendResponse(code=125) # Transfer starting
                    self.logger.log('INFO', get_ident(), 'Writing file to the client...')
                    self.logger.log('INFO', get_ident(), f'Socket is alive: {self.dataConnection.getsockname()}')
                    sent = self.dataConnection.sendfile(data)
                    self.logger.log('INFO', get_ident(), f'Sent {sent} bytes to the client.')
                    return 250 # Successful send
                except:
                    return 425 # Cannot open data connection 
        except:
            return 451 # Action aborted

    def checkAuth(self):
        '''
        Check Authentication Helper

        Checks if the user is autheticated 

        Return
        ------
        `True`: User is authenticated
        `False`: User is not authenticated
        '''
        if self.state != 'AUTH' and self.userLoggedIn:
            self.logger.log('INFO', get_ident(), 'Client has authorization to perform this task...')
            return True
        else:
            self.logger.log('INFO', get_ident(), 'Client does not have authorization to perform this task...')
            self.sendResponse(code=530) # Send user not logged in message 
            return False

    def user(self, username) -> None:
        '''
        USER Command

        Accept the USER Command from the Client with a given username

        Parameters
        ----------
        `username`: str => client's username for the system
        '''
        if self.userLoggedIn:
            self.sendResponse(code=230) # Respond that user is already logged in
        else:
            try:
                with open('accounts.json', 'r') as accounts:
                    users = load(accounts) # Load credentials from JSON
                    for user in users:
                        if username == user["user"]:
                            self.sendResponse(code=331) # Username accepted, password is required
                            self.userName = user["user"]
                            self.logger.log(331, get_ident(), f'Username accepted: {self.userName}')
                            return
                self.sendResponse(code=530) # User is not logged in
            except:
                self.sendResponse(code=451) # Could not read file with usernames

    def password(self, password) -> int:
        '''
        PASS Command

        Accept the PASS Command from the Client with a given password

        Parameters
        ----------
        `password`: str => client's password for the system
        '''
        if self.userLoggedIn:
            self.sendResponse(code=230) # Respond that user is already logged in
        else:
            try:
                with open('accounts.json', 'r') as accounts:
                    users = load(accounts) # Load credentials from JSON
                    for user in users:
                        if user["user"] == self.userName and password == user["pass"]:
                            self.sendResponse(code=230) # Respond that user is now logged in
                            self.passWord = user["pass"]
                            self.userLoggedIn = True # Set logged in 
                            self.state = 'IDLE' # Update state
                            self.logger.log(230, get_ident(), f'Password accepted: {self.passWord}')
                            return
                self.sendResponse(code=530) # User is not logged in
            except:
                self.sendResponse(code=451) # Could not read file with passwords

    def list(self, pathname: str = ''):
        '''
        LIST Command

        Accept LIST Command from the authenticated Client using the custom File System Handler

        Note: Resets the data connection after

        Parameters
        ----------
        `pathname`: str => optional path of directory to list (defaults to current path)
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return
            
        # Assert data connection before transfer
        if not self.dataConnection and (self.state != 'PORT' or self.state != 'PASV'):
            self.sendResponse(code=426) # Connection is closed, operation aborted
            return

        statusCode, tmpPath = self.filesystem.list(pathname)
        self.sendResponse(code=statusCode) # Send Status Code Response to Client
        self.logger.log(statusCode, get_ident(), f'List Command Returned: {tmpPath}')

        # If an error was encountered
        if statusCode != 125 and statusCode != 150:
            self.sendResponse(code=450)
            self.resetDataConnection()
            return

        if tmpPath:
            # Send data to the client
            dataTransferCode = self.sendData(tmpPath)
            self.sendResponse(code=dataTransferCode)
            remove(tmpPath) # Clean tmp file folder
        else:
            self.sendResponse(code=451) # Local error in processing file

        self.resetDataConnection()

    def syst(self):
        '''
        SYST Command

        Accept SYST Command from the Client (Authentication not required)
        '''
        code = 215
        msg = f'System: {name} | {system()} | {release()}'
        self.sendResponse(code=code, msg=msg)
        self.logger.log(code, get_ident(), msg)

    def cdup(self):
        '''
        CDUP Command

        Accept CDUP Command from the Client using the custom File System Handler.
        Simple method that moves the authenticated user up a directory in the file 
        system. For the purposes of this Server, once the user hits `root`, they will
        be barred from navigating any further up the tree for security purposes.
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        code = self.filesystem.cdup()
        self.sendResponse(code=code)
        msg = None
        if code < 500:
            msg = 'File system was successfully updated to the parent directory'
        else:
            msg = 'File system action could not be performed'
        self.logger.log(code, get_ident(), msg)

    def cd(self, pathname: str):
        '''
        CWD/CD Command

        Accept the CWD/CD Command from the Client using the custom File System Handler.
        Simple method that moves the authenticated user to the desired location, which is 
        passed in with `pathname`.

        Parameters
        ----------
        `pathname`: str => desired pathname to switch to (must be a directory)
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        code = self.filesystem.cd(pathname)
        self.sendResponse(code=code)
        msg = None
        if code < 500:
            msg = 'File system was successfully updated to the provided directory'
        else:
            msg = 'File system action could not be performed'
        self.logger.log(code, get_ident(), msg)

    def pwd(self):
        '''
        PWD Command

        Accept the PWD Command from the Client using the custom File System Handler.
        Simple method that reports the current location of the authenticated user 
        within the file system.
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        code, pwd = self.filesystem.pwd()
        self.sendResponse(code=code, msg=pwd)
        msg = None
        if pwd:
            msg = f'Current Directory: {pwd}'
        else:
            msg = 'File system action could not be performed'
        self.logger.log(code, get_ident(), msg)

    def pasv(self) -> None:
        '''
        PASV Command

        Accepts the PASV Command from the Client by creating a server and passing a 
        connection string with an address and port to the authenticated Client.
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        # Assert data connection does not exist before overwriting the connection
        if self.dataConnection or self.state == 'PASV' or self.state == 'PORT':
            self.logger.log('WARNING', get_ident(), 'Data Connection is already established with Client!')
            return

        self.logger.log('INFO', get_ident(), 'Starting Passive Command...')
        try:
            self.logger.log('INFO', get_ident(), f'Addr: {self.addr}')
            passiveConnection = create_server((self.addr, 0))
            addr, port = passiveConnection.getsockname()
            # Port Arithmetic (p1 * 256) + p2
            p1 = port // 256
            p2 = port % 256
            # PASV Connection String
            self.sendResponse(code=227, msg=f'Entering Passive Mode ({addr.replace(".",",")},{p1},{p2})')
            self.state = 'PASV' # Update the state
            self.dataConnection, _ = passiveConnection.accept() # Accept the incoming connection
            self.logger.log(227, get_ident(), 'Passive Command Successful -- Data channel opened with Client.')
        except:
            self.sendResponse(code=425) # Could not open the data connection

    def epsv(self) -> None:
        '''
        EPSV Command

        Accepts the EPSV Command from the Client by creating a server and passing a 
        connection string with an IPv6 Address and Port to the authenticated Client.
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        # Assert data connection does not exist before overwriting the connection
        if self.dataConnection:
            self.logger.log('WARNING', get_ident(), 'Data Connection is already established with Client!')
            return

        self.logger.log('INFO', get_ident(), 'Starting Extended Passive Command...')
        try:
            addr = port = None
            if has_dualstack_ipv6():
                self.logger.log('INFO', get_ident(), 'IPv6 is supported by this machine.')
                ePassiveConnection = create_server((self.addr, 0), family=AF_INET6, dualstack_ipv6=True)
                addr, port, *_ = ePassiveConnection.getsockname()
                connectionStructure = f'|2|{addr}|{port}'
                # EPSV Connection String
                self.sendResponse(code=229, msg=connectionStructure)
                self.state = 'PASV'
                self.dataConnection, _ = ePassiveConnection.accept()
            else:
                self.logger.log('WARNING', get_ident(), 'IPv6 is not supported by this machine.')
                passiveConnection = create_server((self.addr, 0))
                addr, port = passiveConnection.getsockname()
                connectionStructure = f'|1|{addr}|{port}'
                # EPSV Connection String
                self.sendResponse(code=229, msg=connectionStructure)
                self.state = 'PASV'
                self.dataConnection, _ = passiveConnection.accept()
            self.logger.log(229, get_ident(), 'Extended Passive Command Successful -- Data channel opened with Client.')
        except:
            self.logger.log('ERROR', get_ident(), 'Error while connecting to the data socket')
            self.resetDataConnection()
            self.sendResponse(code=425) # Could not open the data connection
            self.logger.log(425, get_ident(), 'Extended Passive Command Failed!')

    def port(self, address: str, p1: str, p2: str):
        '''
        PORT Command

        Accepts the PORT Command from the Client by reading the passed address and ports
        provided by the Client. After performing the proper port arithmetic, a connection 
        can be established with the Client.

        Note: Some assembly required

        Parameters
        ----------
        `address`: str => String of the address 
        `p1`: str => first half of the port
        `p2`: str => second half of the port
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        # Assert data connection does not exist before overwriting the connection
        if self.dataConnection:
            self.logger.log('WARNING', get_ident(), 'Data Connection is already established with Client!')
            return

        self.logger.log('INFO', get_ident(), 'Starting Port Command...')
        try:
            # Convert the String ports to Ints
            port: int = (int(p1) * 256) + int(p2)
            self.logger.log('INFO', get_ident(), f'Attempting to connect to client at {address} on {port}')
            self.dataConnection = socket(AF_INET, SOCK_STREAM)
            self.dataConnection.connect((address, port))
            self.state = 'PORT'
            self.sendResponse(code=200) # Successful Connection
            self.logger.log('INFO', get_ident(), 'Port Command Successful -- Connection opened with Client.')
        except:
            self.logger.log('ERROR', get_ident(), 'Error while connecting to data socket')
            self.resetDataConnection()
            self.sendResponse(code=425) # Could not open the data connection
            self.logger.log(425, get_ident(), 'Port Command Failed!')

    def eprt(self, etype: str, address: str, port: str):
        '''
        EPRT Command

        Accepts the EPRT Command from the Client by reading the passed address and port
        provided by the Client. 

        Parameters
        ----------
        `etype`: str => type of connection that is trying to be established (IPv4 or IPv6)
        `address`: str => string of the Client's address to connect to 
        `port`: str => string of the port value for the process of the Client 
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            return

        # Assert data connection does not exist before overwriting the connection
        if self.dataConnection:
            self.logger.log('WARNING', get_ident(), 'Data Connection is already established with Client!')
            return

        self.logger.log('INFO', get_ident(), 'Starting Extended Port Command...')
        self.logger.log('INFO', get_ident(), f'Attempting to connect to client at {address} on {port}')
        if int(etype) == 1: # If type is 1 => IPv4
            self.dataConnection = socket(AF_INET, SOCK_STREAM)
        else: # If type is 2 (or else) => IPv6
            self.dataConnection = socket(AF_INET6, SOCK_STREAM)
        try:
            self.dataConnection.connect((address, int(port)))
            self.sendResponse(200) # Successful connection
            self.state = 'PORT'
            self.logger.log(200, get_ident(), 'Extended Port Command Successful -- Connection opened with Client.')
        except:
            self.logger.log('ERROR', get_ident(), 'Error connecting via IPv4')
            self.resetDataConnection()
            self.sendResponse(425) # Could not open data connection
            self.logger.log(425, get_ident(), 'Extended Port Command Failed!')

    def stor(self, pathname: str):
        '''
        STOR Command

        Accepts the STOR Command from the Client by reading data through the established
        data connection (PASV/EPSV or PORT/EPRT) with the authenticated Client

        Parameters
        ----------
        `pathname`: str => name of the desired location for the stored file
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            self.resetDataConnection()
            return 

        # Assert data connection exists for transfer to take place
        if not self.dataConnection:
            self.logger.log('ERROR', get_ident(), 'Data connection is not opened!')
            self.sendResponse(code=426) # Data connection is closed
            return

        # Writes the read data to the desired location of the Client
        self.logger.log('INFO', get_ident(), 'Starting the Store Command...')
        status = self.readData(self.filesystem.storFileByPathname(pathname)) # Handles the error codes
        self.sendResponse(code=status)
        self.resetDataConnection()
        self.logger.log(status, get_ident(), self.error.getByCode(status))

    def retr(self, pathname: str):
        '''
        RETR Command

        Accepts the RETR Command from the Client by writing data through the 
        established data connection (PASV/EPSV or PORT/EPRT) with the authenticated
        Client.
        '''
        # Assert Client's auth status
        if not self.checkAuth():
            self.resetDataConnection()
            return 

        # Assert data connection exists for transfer to take place
        if not self.dataConnection:
            self.logger.log('ERROR', get_ident(), 'Data connection is not opened!')
            self.sendResponse(code=426) # Data connection is closed 
            return

        self.logger.log('INFO', get_ident(), 'Starting the Retrieval Command...')
        pathname = self.filesystem.retrFileByPathname(pathname)
        if pathname:
            self.logger.log('INFO', get_ident(), f'Attempted to retr from {pathname}')
            status = self.sendData(pathname) # Handles the error codes
            self.sendResponse(code=status)
            self.resetDataConnection()
            self.logger.log(status, get_ident(), self.error.getByCode(status))
        else:
            self.sendResponse(code=550) # File unavailable
            self.resetDataConnection()
            self.logger.log(550, get_ident(), self.error.getByCode(550))

