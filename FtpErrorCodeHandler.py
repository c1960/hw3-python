#!/usr/bin/python3

# CS 472 - HW3
# Christopher S. Good, Jr. 
# FtpErrorCodeHandler.py

class FtpErrorCodeHandler:
    '''
    FTP Error Code Handler

    Simple class for storing and accessing Error Codes and messages.
    This class abstracts the error code dictionary away from the user and
    makes it easier to pass around and use
    '''

    def __init__(self):
        self.index = {
            110: 'Restart marker reply.',
            120: 'Service ready in nnn minutes.',
            125: 'Data connection open; transfer starting.',
            150: 'File status okay; about to open data connection.',
            200: 'Command okay.',
            202: 'Command not implemented, superfluous at this state.',
            211: 'System status, or system help reply',
            212: 'Directory status.',
            213: 'File status.',
            214: 'Help message.',
            215: 'NAME system type.',
            220: 'Servive ready for new user.',
            221: 'Service closing control connection.',
            225: 'Data connection open; no transfer in progress.',
            226: 'Closing data connection.',
            227: 'Entering Passive Mode (a1,a2,a3,a4,p1,p2).',
            230: 'User logged in, proceed.', 
            250: 'Requested file action okay, completed.',
            331: 'User name okay, need password.', 
            332: 'Need account for login.', 
            421: 'Service not available, closing control connection.', 
            425: 'Cannot open data connection.', 
            430: 'Invalid username or password.', 
            450: 'Requested file action not taken.',
            451: 'Requested action aborted. Local error processing.',
            501: 'Syntax error in parameters or arguments.',
            502: 'Command not implemented.', 
            503: 'Bad sequence of events.',
            530: 'Not logged in.',
            550: 'Requested action not taken. File unavailable.'
        }

    def getByCode(self, code: int) -> str:
        return self.index.get(code)
