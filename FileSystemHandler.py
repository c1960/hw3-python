#!/usr/bin/python3

# CS 472 - HW3
# Christopher S. Good, Jr. 
# FileSystemHandler.py

import os
from threading import get_ident
from typing import Tuple
from Logger import Logger


class FileSystemHandler:
    '''
    File System Handler

    Wrapper for a custom File Handler designed to abstract the core functionalities away from the 
    user and the FTP Service. By using this custom class, the FTP Server does not have to maintain 
    it's own awareness of the files that it is managing, while also being more secure, since the user
    does not have access to the files that make up the Server (This adds more security, since an attacker
    cannot `download` the source code and use it to exploit the system). 
    '''

    def __init__(self, root='root', tmp='tmp', logger=Logger()):
        '''
        Initializes the File System Handler

        Parameters
        ----------
        `root`: str => the root folder of the custom file system (Defaults to 'root')
        `tmp`: str => the folder where all temporary files (for LIST, etc.) will be written to 
        `logger`: Logger => custom logger for the file system handler (Defaults to the Server's logger)
        '''
        self.tmp = tmp
        self.root = root
        self.current = root
        self.logger: Logger = logger

    def exists(self, pathname: str) -> bool:
        '''
        Asserts if a file exists in the scope of the filesystem

        Parameters
        ----------
        `pathname`: str => name of the file to find

        Return
        ------
        `True`: bool => file exists
        `False`: bool => file does not exist
        '''
        return os.path.exists(os.path.relpath(pathname, self.root))

    def cdup(self) -> int:
        '''
        Changes the current directory to that of it's parent directory 

        Note: If the current directory is `root`, then the file system 
        will maintain it's position at `root`

        Return
        ------
        `250`: Successful directory change
        `550`: Requested action failed
        '''
        try:
            pathname = os.path.relpath(self.current, self.root)
            pathname = pathname.split(os.path.sep)
            pathname = self.current.split(os.path.sep)
            if len(pathname) > 1:
                self.current = os.path.sep.join(pathname[0:-1])
            else:
                self.current = self.root
            return 250  # Successful directory change
        except:
            return 550  # Requested action failed

    def cd(self, pathname: str) -> int:
        '''
        Changes the current directory to another directory within the 
        confines of the `root` directory tree

        Parameters
        ----------
        `pathname`: str => name of the directory to change to 

        Return
        ------
        `250`: Successful directory change
        `550`: Requested action failed
        '''
        if os.path.isdir(f'{self.current}/{pathname}'):
            self.current += f'{os.path.sep}{pathname}'
            return 250  # Successful directory change
        else:
            return 550  # Requested action failed

    def pwd(self) -> Tuple[int, str]:
        '''
        Reports the current directory to the user

        Return
        ------
        `code`: int => status code of the file system action 
        `location`: str => current location in the filesystem
        '''
        try:
            return 257, self.current  # Successful current directory report
        except:
            return 550, None  # Requested action failed

    def list(self, pathname: str) -> Tuple[int, str]:
        '''
        Handles the LIST or LS Command

        This method writes the contents of the current directory into a
        temporary file.

        Parameters
        ----------
        `pathname`: str => optional value of a specific path to list out

        Return
        ------
        `code`: int => return code corresponding to the outcome of the tmp file
        `path`: str => name of the tmp file that the directory was written to
        '''
        try:
            dirList = None
            if pathname:
                dirList = pathname  # Set directory to optional pathname
            else:
                dirList = self.current  # Set durectory to current directory

            # Assign a temp file (Uniquely tied to the current Thread ID)
            tmpPath = f'tmp/dir_{get_ident()}'
            with os.popen(f'ls -l {dirList}') as directory:
                with open(tmpPath, 'w') as data:
                    for file in directory:
                        data.write(file)
            return 150, tmpPath  # File written successfully
        except:
            return 451, None  # Local file handling error

    def storFileByPathname(self, pathname) -> str:
        '''
        Handles the STORE Command

        This method returns the current relative path for the file to be saved within
        the confines of the custom files ystem (root)

        Parameters
        ----------
        `pathname`: str => desired location for the stored file

        Return
        ------
        `path`: str => full path from `root` 
        '''
        return f'{self.current}/{pathname}'

    def retrFileByPathname(self, pathname) -> str:
        '''
        Handles the RETR Command

        This method returns the current relative path for the file that the Client
        is trying to access from the custom file system (root)

        Parameters
        ----------
        `pathname`: str => location or name of the desired file

        Return 
        ------
        `path`: str => path to the desired file
        '''
        if pathname in os.listdir(self.current):
            return f'{self.current}/{pathname}'
        else:
            return None

