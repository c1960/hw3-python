# CS 472 - HW3
Christopher S. Good, Jr. 
FTP Server Program
Python 3
Assigned: October , 2021
Completed: November 3, 2021

# Project Dependencies
This project uses Python 3.10 for:
- Better typing structures
- Socket features

# Project Structure
This program is broken up into X main parts:
- main.py:
    This is the main function that drives the Server and handles the user's input
    Note: The server runs as it's own subprocess and therefore the main program can 
    be used to kill server at any time
- FtpServer.py:
    Server which implements the Thread Class. The core `run` method takes care of all
    the overhead of spinning up new Services that handle the various Clients' requests.
- FtpService.py:
    Service which implements the Thread Class. The core `run` method addresses all the 
    core functionality of the assigned components of the FTP Server's Service.
- FileSystemHandler.py:
    File System Handler that abstracts the various IO/File System Operations away from 
    the user and the Server. Additionally, it adds a layer of organization and security 
    to the project, as it keeps the files and source code separate, while also maintaining
    a familiar Linux structure.
- Logger.py:
    Custom Logger that directs program output to either a file, the screen, or both
    Note: The default for the Server is `BOTH`.
- FtpErrorCodeHandler.py:
    Simple Class with an accessor to capture particular messages from error codes. This class
    simplifies the responses sent to the Client and assures uniform behavior.
- accounts.json:
    JSON file that houses the Usernames and Passwords for the system. There are currently two 
    users in the system (myself and Vivek (For fun/grading purposes))
- SampleLogs:
    Collection of sample log txt files that contain output from various tests of the system:
    - ftp-client-hangs-up-poorly.txt: Testing how the server manages threads when the client doesn't 
      send a proper QUIT command and just "disappears"
    - ftp-concurrent-hanging-connections.txt: Testing how the server manages two clients who
      do no hang up their data connections. The Server handles the cleanup of those connections.
    - ftp-concurrent-users.txt: Showcases how the server can handle two users simultaneously and 
      shows how this would work at scale without crossing passwords or commands between two users
    - ftp-data-commands-left-open.txt: Testing how the server responds to a single client with 
      a hanging connection
    - ftp-data-commands.txt: Showcases the PASV, EPSV, PORT, and EPRT commands that the server has
      implemented between the various tasks of STOR, RETR, and LIST
    - ftp-server-stops-with-client-attached: Similar error handling, but with a single control 
      connection
    - ftp-simple-commands-with-errors.txt: Showcases the error handling that is build into the server, 
      when the client sends back unordered or unwanted or unimplemented commands
    - ftp-simple-commands.txt: Showcases the simple commands CWD, CDUP, SYST, LIST, USER, PASS, 
      QUIT, and PWD
- root:
    Simple directory that serves as the base (or root) of the custom File System. For this excercise, I
    will include some test files that I used while building my sample logs. (For good measure, I threw
    an XLSX file to make sure the types were working fine)
- tmp:
    Simple directory where any temporary files that the Server or File System need live. This is purposefully
    out of the scope of the other files, since it contains Thread IDs that could be exploited.

# Running the Program 
To run the program please provide (at the least) an IP Address and Port Pair where you would 
like the Server to live.
Then, run the following command to execute the project:
`python3 main.py -a XXX.XXX.XXX.XXX -p 2121`
or 
`./main.py -a XXX.XXX.XXX.XXX -p 2121`

Note: The default for this Server Application is the Native Port for FTP (21)
If you would like to run this program on that port, then please add the `sudo`
command before the filename:
`sudo python3 main.py -a XXX.XXX.XXX.XXX`
or 
`sudo ./main.py -a XXX.XXX.XXX.XXX`

This program is also designed to accept three different flags:
- a: IP Address for the Server to live on 
- p: Port Number for the Server to bind to 
- lf: Name of the desired Log File 

# Stopping the Program
To stop the program from the main process, type `kill` into the window and the main 
process will pick up the command and execute the Server's `sendStop()` method. This method
will run through the open connections and terminate them, all while joining all the Threads and 
the Server's main Thread to the main process. Once this happens, the program should exit gracefully. You 
will also be able to see when and where classes move in and out of scope at the end of this process.

# Logging
The program follows the general logging patter of: 
`MM/DD/YY HH:MM:SS LEVEL (THREADID) MESSAGE`

## Level
- Custom: String is passed from the Server (Designed for PASV/EPSV)
- INFO: 0 < Error Code < 220
- SUCCESS: 220 < Code < 299
- PENDING: 300 < Code < 399
- WARNING: 400 < Code < 499
- ERROR: 500 < Code < 650

## ThreadId
This is captured by the built in `get_ident()` function that the Threading library provides. This returns
the Thread ID of the calling process, which allows each Thread to share the same logger and same output file, 
while also having different corresponding Thread IDs

## Message
This message usually corresponds to a typical code, but in some cases it can be custom. For instance, the 
PASV and EPSV methods will return a custom string that the client uses to connect to the Server.

# FTP Server
The FTP Server, which is an extension of the Thread class, is designed to run asynchronously from the 
main thread, so that start and stop commands can be send as signals. This is an effective way to manage 
the Server, since it removes a lot of the stopping conditions from the main process and makes the 
code overall cleaner. 
## Starting
To start the Server, you can use the basic Thread `start()` method, which will execute the `run()` method
in the Server Class. 
## Stopping
To stop the Server, you can use the `sendStop()` method, which breaks the main execution loop of the 
Server and handles the cleanup of any extra Threads or Connections laying around.
## Running
The main process of the Server is essentially a Thread Pool. The method will wait and accept any incoming
connections from Clients and then pass those connections off to Sub-Threads (FTP Services) where the FTP 
Commands are implemented. This extra level of abstraction allows the server to focus solely on handling the 
different incoming connections, rather than also handling the FTP Commands. Furthermore, all Threads are 
tracked in the `connections` list, which is cleaned up with the `close()` method.

# FTP Service
The FTP Service, which is also an extension of the Thread class, is designed to be spawned for each connection
that the Server accepts. These Services run in tandem and having each on their own Thread (with their own data 
members and so-on) keeps them organized and reduces the headache of having to track variables and values accross
Threads (since each Service is the base component of the FTP Protocol on the Server Side). This Service also keeps
track of the State of the current process, with different nodes being 'AUTH', 'IDLE', 'PORT', 'PASV', and
"symbolic" 'CLOSED'.
## Starting
Each Service Thread is started automatically when the Server receives a connection.
## Stopping
To stop the Service Thread, you can send a `QUIT` command from the Client or you can close the Server. Either 
of these actions will cause the Thread to terminate it's main process.
## Running
The main process of the Servive is essentially the opposite side of the Client's main process, as it reads in 
commands from the Client and then runs through a decision tree of where to push those commands. Each implemented
command is associated with it's own method in the Service:
- user: Accepts the USER command from the Client (State: AUTH)
- password: Accepts the PASS command from the Client (State: AUTH)
- port: Accepts the PORT command from the Client (State: PORT)
- eprt: Accepts the EPRT command from the Client (State: PORT)
- cwd: Accepts the CWD command from the Client (State: IDLE)
- stor: Accepts the STOR command from the Client (State: PORT/PASV)
- retr: Accepts the RETR command from the Client (State: PORT/PASV)
- pasv: Accepts the PASV command from the Client (State: PASV)
- epsv: Accepts the EPSV command from the Client (State: PASV)
- list: Accepts the LIST command from the Client (State: PORT/PASV)
- pwd: Accepts the PWD command from the Client (State: IDLE)
- syst: Accepts the SYST command from the Client (State: IDLE)
- cdup: Accepts the CDUP command from the Client (State: IDLE)
- quit: Accepts the QUIT command from the Client (State: AUTH/IDLE/PORT/PASV)

# Testing
For testing purposes, I am going to supply my client in a zipped folder. I did some testing with python's ftplib and 
was very successful with the USER and PASS commands, but the data commands were more tricky. (Their client seems to 
have a method that takes in the command, but doesn't somehow pass it back to the Server?). Additionally, I implemented
the CDUP command in he program that it can be tested easily.

My Client will be in a folder labeled `FtpClient`

# Questions

## Question 1
I feel that this Server is definitely hackable, since the basic implementation of the Protocol is widely known. For 
instance, in CS 377 we have learned that you can execute commands from environment variables in Bash scripts (or older
versions of Bash scripts) and since this is dealing with the Python OS module and other different IO/File commands, there
is probably a high chance that this Server is hackable. 

While I was building this Server, I tried to keep my White Hat on, and plan around some of the basic attacks that I could 
think of. One basic one would be storing the files with the Program (.py) files -- since this would allow for anyone to 
come in an RETR those files and look for ways to exploit the program first-hand. To combat this, I build a FileSystemHandler
that abstracts some of the IO/File operations from the Server. One of the other things that it does is it keeps the user
inside the 'root' or other designated folder when running the Server and browsing files. For instance, a User cannot just 
simply CDUP into my program files, since there are some safeguards in the way that the directory is tracked that would prevent
that from happening; however, that is not to say that it is impossible. 

In terms of Root Shell, one way that you could compromise this system would be through an attack on a Set-UID program that 
replaces the `ls` command that I call to grab the files in the folders. If an attacker were to replace this with a custom
version of `ls` in the filesystem and find a way to symlink it to the path, they could possibly get Root Shell from there. 

Another, more networky answer, would also be that someone could listen in at the Specific address of the Server and guess 
around for some ports. One thing that I've noticed while doing many hours of testing is that you can sometimes predict the 
range of the upcoming ports that the system will free-up when doing commands back-to-back. Since the Server is running on one
machine, it would not be a stretch for someone to try a guess and check, or listen over a block of ports to try and see what 
someone is sending across the network. 

All in all, I would say that since the protocols are very well defined and since the nature of the achieving the specific
tasks in the Server are quick general (and known), it would not be a stretch to compromise this Server with some elbow grease.

## Question 2
One of the more interesting, and useful elements of the FTP protocol would be the separate control and data connections. While
it can sometimes be uninituitive to close these after every file is transfered or action is complete, it is actually quite
useful to be able to anticipate what is going to be coming across each "pipe". For instance, when listening for commands, you
know that you are going to get a 2/4 character command followed by a single space and a message, while on the data connection
you can listen for much larger amounts of data without having to worry about context switching for an expected command. 

One of the weird things that I've noticed would be centered around the sequence of messages that are sent back and forth. While
they are pretty well structured, and it makes sense why they have to be in that specific order, it is nevertheless weird 
that you have to have two different methods of connecting a data connection, each with a different ordering of messages. 
On one hand, this could be seen as a perk, since it gives you customization over your experience, however, I would say that 
this is something that can just confuse the system and makes it weird to implement. (For one note, it was tricky to get this
ordering right in Python where a lot of the nastiness is abstracted away, but in a lower-level language this would probably
be a more present issue).

## Question 3
Similar to my statements above, I feel that it is convenient to have two sockets handling the separate tasks; however, it 
most likely is not impossible to handle this with one connection. For starters, you could just the same `sendfile` function 
to handle the data throughput and the `makefile` function to handle reading the data. This would most likely work for both commands
and data files without too much modification. From there, there the tricky part would be getting the proper order down, in 
particular for the PASV and PORT commands, since they would be trying to do the same thing, but in different orders. 

All in all, I feel that with the way that I've broken down this project, it would be relatively feasible to make it happen over 
one socket, but there would definitely be some nuances and tricks.