#!/usr/bin/python3

# CS 472 - HW3
# Christopher S. Good, Jr. 
# main.py

# Main Program that runs the FTP Server
# To kill this program (and the FTP Server), enter `kill` into 
# the commandline anytime during runtime and the server will 
# start it's clean-up routine

from ctypes import addressof
import sys
from typing import Tuple
from FtpServer import FtpServer
from Logger import Logger

def handleCommandLineInput():
    addr = port = lf = None

    if '-a' in sys.argv:
        try:
            addr_index = sys.argv.index('-a')
            addr = sys.argv[addr_index + 1]
        except:
            print('Could not find valid address argument...')
    if '-p' in sys.argv:
        try:
            port_index = sys.argv.index('-p')
            port = int(sys.argv[port_index + 1])
        except:
            print('Could not find valid port argument...')
    if '-lf' in sys.argv:
        try:
            lf_index = sys.argv.index('-lf')
            lf = sys.argv[lf_index + 1]
        except:
            print('Could not find valid log file argument...')

    return addr, port, lf

if __name__ == "__main__":

    addr, port, lf = handleCommandLineInput()

    # Create an instance of a Logger
    logger: Logger = None
    if lf:
        logger = Logger(lf)
    else: 
        logger = Logger()

    # Create an instance of an FTP Server
    server = None
    if addr and port:
        server = FtpServer(address=addr, port=port, logger=logger)
    elif addr:
        server = FtpServer(address=addr, logger=logger)
    elif port:
        server = FtpServer(port=port, logger=logger)
    else:
        server = FtpServer()

    # Start Server Thread
    server.start() 

    while True:
        kill = input('Terminate? (KILL): ') # Wait for Kill Command
        if kill.upper() == 'KILL':
            server.sendStop()
            server.join()
            break # Break from main execution loop
        